import 'dart:math';

import 'package:flutter/material.dart';
import 'dart:math' as math;

class LinePainter extends CustomPainter {
  double _progress = 0.9;

  LinearGradient lineColor;
  Color completeColor;
  double startPercent;
  double endPercent;
  double width;
  int startValue;
  int endValue;
  final angle = 2 * pi / 60;

  double value;

  LinePainter(
      {this.lineColor,
      this.completeColor,
      this.startValue,
      this.endValue,
      this.startPercent,
      this.endPercent,
      this.width,
      this.value});
  @override
  void paint(Canvas canvas, Size size) {
    final rect = new Rect.fromLTWH(0.0, 0.0, size.width, size.height);
    final gradient = new SweepGradient(
      startAngle: 3 * math.pi / 4,
      endAngle: 2.8 * math.pi,
      tileMode: TileMode.repeated,
      colors: [
        Colors.red[800],
        Colors.red[700],
        Colors.red[600],
        Colors.red[500],
        Colors.red[400],
        Colors.yellow[900],
        Colors.yellow[800],
        Colors.yellow[700],
        Colors.yellow[600],
        Colors.yellow[500],
        Colors.yellow[400],
        Colors.yellow[300],
        Colors.green[100],
        Colors.green[200],
        Colors.green[300],
        Colors.green[300],
        Colors.green[400],
        Colors.green[400],
        Colors.green[400],
        Colors.green[500],
        Colors.green[500],
        Colors.green[600],
        Colors.green[600],
        Colors.green[700],
        Colors.green[700],
        Colors.green[700],
        Colors.green[800],
        Colors.green[800],
        Colors.green[900],
        Colors.green[900],
      ],
    );

    final paint = new Paint()
      ..shader = gradient.createShader(rect)
      ..strokeCap = StrokeCap.butt // StrokeCap.round is not recommended.
      ..style = PaintingStyle.stroke
      ..strokeWidth = width;
    final center = new Offset(size.width / 2, size.height / 2);
    final radius = math.min(size.width / 2, size.height / 2) - (width / 2);
    final startAngle = 0.8 * math.pi;
    final sweepAngle = 1.4 * math.pi;
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius),
        startAngle, sweepAngle, false, paint);

    // Paint line = new Paint()
    //   ..color = lineColor
    //   ..strokeCap = StrokeCap.butt
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = width;
    // Paint complete = new Paint()
    //   ..color = completeColor
    //   ..strokeCap = StrokeCap.butt
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = width;

    // Offset center = new Offset(size.width / 2, size.height / 2);
    // double radius = min(size.width / 2, size.height / 2);

    // canvas.drawArc(new Rect.fromCircle(center: center, radius: radius),
    //     25 * angle, 40 * angle, false, line);

    // canvas.drawArc(
    //     new Rect.fromCircle(center: center, radius: radius),
    //     25 * angle + (40 * angle) * startPercent,
    //     (40 * angle) * (endPercent - startPercent),
    //     false,
    //     complete);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
